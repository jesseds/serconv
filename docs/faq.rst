FAQ
===

Doesn't program X or plugin Y do the same thing?
------------------------------------------------
There are other programs and plugins capable of converting TIA ser files to various degrees. At the time that SERConv was created, there 3 were situations where other solutions were lacking:

1. The conversion of spectrum images. Some programs do not support the higher-dimensional nature of spectrum images properly. I.e., a spectrum image will be opened as separate images (each slice), or not open at all.
2. A change in the bit-precision stored in ser files beginning from about TIA version 4.7.3 is not compatible with certain software. This change allowed for files greater than 2 GiB, but broke many converters.
3. Batch conversion of and searching of arbitrary directories/sub-directories and/or specific combinations of series files, similar to the GMS batch conversion feature for dm3 files.

Can I convert emi files instead of ser?
---------------------------------------
No, emi files are proprietary to FEI. Since they have not released the file format specification it is not possible for any program to convert these; thus, it is not advised to delete ser files. Sometimes people like to delete the ser files since they contain duplicated information as the emi; howevever, this is not always the case. For spectrum images, TIA will *only* write the raw data to the ser file.

Why do images appear black or washed out?
-----------------------------------------
Many generic image viewing programs only support 8 bit-depth images. The data types and precision saved in a ser file can be of multiple different data types and/or precisions from 8 to 64 bits. This causes many image viewing programs to not know how to display images correctly (if greater than 8-bit). The solution is to open/process the image in programs known to support such greater data types, such as ImageJ or DigitalMicroph/Gatan Microscopy Suite.

For greater ease in viewing images, an option is provided to covert all images to 8-bit.


How do I know the spatial calibration?
--------------------------------------
SERConv intentionally does not burn a scale bar into the image, as it is not intended to be used as an image processor (or alter the raw data in any way). Instead, the spatial calibration is embedded in the image metadata in units of nanometers (reciprocal and energy units are not embedded). It is up to the user to process the image how they desire.

For example, below is an electron energy loss spectrum image (EELS-SI) converted using SERConv. The tif file is loaded into image, and the spatial calibration is automatically set:

.. image:: imgs/spatial_calibration.png
    :alt: ImageJ window of automatically calibrated EELS spectrum image
    :align: center
    :scale: 66
