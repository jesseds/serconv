Converting SER files to TIFF
============================

Individual files and directories
--------------------------------
Individual ser files can be added either by drag-and-drop from a file explorer or using the "Add file(s)" button. Any number of files can be queued for conversion.

Individual folders can similarly be added by drag-and-drop or the "Add folder" button. Adding a folder tells SERConv to convert all ser files in the folder. Both folders and files can be simultaneously queued, as seen in the image below:

.. image:: imgs/file_folder.png
    :align: center
    :scale: 90

Note that emi files are proprietary to FEI and cannot be converted (so don't delete the ser files).

Removing items
--------------
Currently selected item can be removed from the queue using the "Remove button", all items can be removed using the "Clear" button.

Recursive searching
-------------------
The "Include sub-folders" option in the right panel tells SERConv to recursively search all subfolders in each folder for ser files to convert.

8-bit conversion
----------------
The "Convert to 8-bit" option will downcast all data into 8-bit integer type. This allows images to more easily be viewed in non-technical programs, but increases the risk of banding (see: https://en.wikipedia.org/wiki/Colour_banding ).

Overwrite existing files
------------------------
SERConv can be told whether or not to overwrite existing tiff files, this can be set with the "Overwrite existing" button.

Adding suffix to converted files
--------------------------------
The suffix box in the options pane allows for adding a suffix to the filename of each converted file. This might be useful if creating both native bit-depth tiffs as well as 8-bit downscaled tiffs (i.e., by adding "_8bit" to the field)..
