Download and installation
=========================

Windows
-------
An installer is provided for Windows users, and can be downloaded at https://gitlab.com/jesseds/serconv/-/releases

Linux & MacOS
-------------
Compiling from source requires a c++ 17 capable compiler. Linux & MacOS users will have to compile SERConv from source. Compilation has not been tested on Mac systems so your mileage may vary. Compilation on Linux is only tested using GCC and MingW on Windows.

Clone the repository::

    git clone https://gitlab.com/jesseds/serconv

Some dependencies will have to be compiled or downloaded and their installation source linked to CMake. There are 3 dependencies:

1. libtiff (https://gitlab.com/libtiff/libtiff )
2. Qt6 (https://www.qt.io/ )
3. OpenSSL (https://www.openssl.org/ )
