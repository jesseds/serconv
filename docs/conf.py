# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
import re
from pathlib import Path


cmake_lists = Path(os.path.abspath(os.path.dirname(__file__))) / ".." / "CMakeLists.txt"

with open(cmake_lists, "r") as file:
    txt = file.read()
    major = re.findall(r'"MAJOR_VERSION".*"(\d+)"', txt)
    minor = re.findall(r'"MINOR_VERSION".*"(\d+)"', txt)
    patch = re.findall(r'"PATCH_VERSION".*"(\d+)"', txt)

    assert len(major) == 1, "Found more than 1 major version match"
    assert len(minor) == 1, "Found more than 1 minor version match"
    assert len(patch) == 1, "Found more than 1 patch version match"

    major = major[0]
    minor = minor[0]
    patch = patch[0]

    version_list = (major, minor, patch)
    version_str = f"{major}.{minor}.{patch}"

    print(f"SERConv version is", version_str)



# -- Project information -----------------------------------------------------

project = "SERConv"
copyright = "2019-2022, Jesse Smith"
author = "Jesse Smith"

# The full version, including alpha/beta/rc tags
release = version_str


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx_sitemap"
]


# Add any paths that contain templates here, relative to this directory.
# templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

set_type_checking_flag = False

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
html_theme = "sphinx_material"
html_theme_options = {
    "globaltoc_depth": 2,
    "repo_name": "Batch ser to tiff conversion",
    "nav_title": "SERConv",
    "color_primary": "blue",
    "globaltoc_collapse": False,
    "repo_type": "gitlab",
    "color_accent": "cyan",
    "master_doc": True,
    "repo_url": "https://gitlab.com/jesseds/serconv",
}

pygments_style = "sphinx"
html_logo = "imgs/logo_material.svg"
html_sidebars = {"**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]}
# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_extra_path = ['google9a0e44d229763c5f.html', "robots.txt"]
html_baseurl = "https://jesseds.gitlab.io/serconv"
sitemap_url_scheme = "{link}"

