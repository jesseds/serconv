SERConv: Batch TIA ser to tiff file conversion
================================================

.. image:: imgs/serconv.png
    :alt: Main window of SERConv during file conversion
    :align: center
    :scale: 66

**SERConv is a utility for easy batch conversion of ser files from TIA (Emispec/ESVision) microscopy software to the tiff format.**

SERConv provides a solution for batch conversion of TIA output (ser/emi) to tiff format. Output from SERConv is primarily intended for use in ImageJ and DigitalMicrograph, but should work for any application capable of reading tiff files. SERConv supports conversion of basic 2d imagery (i.e., STEM images, diffraction patterns, brightfield, etc), while also supporting hyperspectral formats such as line scans and spectrum images. Support for spectrum images is critical to enable analysis of electron energy loss (EELS) and energy dispersive Xray spectroscopy (EDS) maps in DigitalMicrograph or other programs.

* Support for conversion of multi-dimensional data (spectra, imagery, and diffraction patterns).
* Direct support of hyperspectral EELS (spectrum imaging).
* Batch conversion.
* Support both 32- and 64-bit byte offsets that depend on TIA versions.
* Retains spatial calibration as tiff metadata
* Optional conversion to 8-bit depth.
* Optional recursive directory searching for ser files.
* Optional suffix to file names.


Download & Installation
-----------------------
Windows installers can be found at: https://gitlab.com/jesseds/serconv/-/releases .
Non windows users should look at the download and installation page for manual compilation.

.. toctree::
   :maxdepth: 2
   :hidden:

   install
   converting_ser_files
   faq
   license
