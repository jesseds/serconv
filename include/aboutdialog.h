/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#pragma once

#include <QDialog>
#include <QVBoxLayout>
#include <QLabel>
#include <QTextEdit>
#include <QTabWidget>
#include <QDialogButtonBox>

class AboutDialog : public QDialog {
    Q_OBJECT
public:
    explicit AboutDialog(QWidget* parent=0);

private:
    QTabWidget* tabs;
    QWidget* about_widget;
    QVBoxLayout* toplayout;
    QVBoxLayout* aboutlayout;
    QLabel* serconv_lbl;
    QLabel* link;
    QLabel* copyright;
    QLabel* description;
    QTextEdit* license;

    QDialogButtonBox* btns;
};
