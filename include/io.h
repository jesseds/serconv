/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#pragma once

#include <string>
#include <fstream>
#include <vector>
#include <QString>

struct DataND {
    uint16_t DataType;
    std::vector<double> Data;

    size_t virtual DataTypeSize();
};

struct Data1D : DataND {
    double CalibrationOffset;
    double CalibrationDelta;
    uint32_t CalibrationElement;
    uint32_t ArrayLength;
};

struct Data2D : DataND {
    double CalibrationOffsetX;
    double CalibrationDeltaX;
    int32_t CalibrationElementX;
    double CalibrationOffsetY;
    double CalibrationDeltaY;
    int32_t CalibrationElementY;
    uint32_t ArraySizeX;
    uint32_t ArraySizeY;
};


struct DimensionArray {
    uint32_t DimensionSize;
    double CalibrationOffset;
    double CalibrationDelta;
    int32_t CalibrationElement;
    uint32_t DescriptionLength;
    std::string Description;
    uint32_t UnitsLength;
    std::string Units;
};


struct SerData {
    int16_t ByteOrder;               // only 0x4949 i.e. little endian
    int16_t SeriesID;                // only 0x0197
    int16_t SeriesVersion;           // 0x0220 (TIA version >= 4.7.3) otherwise 0x0210
    int32_t DataTypeID;              // 0x4120 for 1D arrays, 0x4122 for 2D arrays
    int32_t TagTypeID;               // 0x4152 tag is time only, 0x4142 2D pos with time
    int32_t TotalNumberElements;     // Total number of data elements/tags
    int32_t ValidNumberElements;     // Total number of valid data elements
    int64_t OffsetArrayOffset;       // byte offset that the data offset array starts
    uint32_t NumberDimensions;        // Number of dimensions (indicies)

    std::string spatial_unit = "";

    std::vector<DimensionArray> Dimensions;
    std::vector<int64_t> DataOffsetArray;
    std::vector<int64_t> TagOffsetArray;
    std::vector<Data1D> Data_1D;
    std::vector<Data2D> Data_2D;

//    double MinVal;
//    double MaxVal;
};

template<class T>
T read(std::ifstream& ifile);

template<class T>
std::vector<double> read_sequence(std::ifstream& ifile);

std::string read_string(std::ifstream& ifile, size_t size);
std::pair<double, double> find_min_max(std::vector<DataND> data);

SerData read_ser(std::string& fpath);
SerData read_ser(QString& fpath);



