#pragma once

#include <QAbstractListModel>
#include <QFileInfoList>
#include <QtQml/qqmlregistration.h>


QFileInfoList accumulate_ser_paths(QDir& dir, bool subdirs);


class Entry {

public:
    explicit Entry(QString path, bool subdirs);

    auto path() const {return QString(_path.absoluteFilePath());}
    QString name() const;
    QString descr() const;

    bool subDirs() const {return _subdirs;};
    void setSubDirs(bool);

    bool active() const {return _active;}
    void setActive(bool val);

    bool isDir() const {return _path.isDir();}
    auto numSerFiles() const {return _ser_files.size();}

private:
    bool _subdirs = false;
    QFileInfo _path {};
    QFileInfoList _ser_files {};
    bool _active = true;

    void _updateSerList();
};


class EntryModel : public QAbstractListModel {
    Q_OBJECT

public:
    explicit EntryModel(QObject* parent = nullptr);

    enum EntryRoles {
        Path = Qt::UserRole + 1,
        Descr,
        Name,
        Active,
        SubDirs // This role does not get displayed, but for notifying the ui that subdirs changed
    };

    void addEntry(Entry*);
    auto entries() const {return _entries;}
    void setSubDirs(bool);
    Q_INVOKABLE void removeEntry(QString& path);
    Q_INVOKABLE void clear();
    int rowCount(QModelIndex const& parent = QModelIndex()) const override {return _entries.size();}
    QVariant data(const QModelIndex & index, int role) const override;
    bool setData(QModelIndex const&, QVariant const&, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(QModelIndex const&) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    std::vector<Entry*> _entries {};
};

