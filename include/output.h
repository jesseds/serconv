/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#pragma once

#include <string>
#include <vector>
#include <QString>
#include <QFileInfo>

#include <io.h>


using std::vector;
using std::string;


void write_spectrum_image(string& fpath, vector<DimensionArray>& dims, vector<Data1D>& indata, std::string unit="");
void write_linescan(string& fpath, vector<Data1D>& indata, std::string unit="");
void write_spectrum(string& fpath, Data1D& indata, uint row, std::string unit="");
void write_image(string& fpath, Data2D& indata, bool t08bit=false, std::string unit="");

std::optional<std::string> map_writer(string& ser, string& fpath, bool to8bit=false);
std::optional<std::string> map_writer(QString& ser, QString& fpath, bool to8bit=false, QString const& suffix="");

std::string image_description(int x, int y, int pages, std::string unit);

QFileInfo convert_path_ext(QFileInfo const& fpath);
