#pragma once

#include <QAbstractListModel>
#include <QFileInfoList>


class Prog {

public:
    explicit Prog(QFileInfo const& path);

    enum Status {
        Queued,
        Successful,
        Failed,
        Skipped,
        Canceled,
        Working
    };

//    auto path() const {return QString(_path.absoluteFilePath());}

//    QString name() const;

//    QString descr() const;

//    bool active() const {return _active;}
//    void setActive(bool val);

//    bool isDir() const {return _path.isDir();}
//    auto numSerFiles() const {return _ser_files.size();}

    void setResult(QString const&);
    auto result() const {return _result;}

    void setStatus(Status);
    auto status() const {return _status;}

    QString statusStr() const;

    auto path() const {return _path;}

private:
    QFileInfo const _path {};
    QString _result = "";
    Status _status = Status::Queued;
};


class ProgModel : public QAbstractListModel {
    Q_OBJECT

public:
    explicit ProgModel(QObject* parent = nullptr);

    enum ProgRoles {
        Path = Qt::UserRole + 1,
        Status,
        Result
    };

    void addProg(Prog*);
    auto progs() {return _progs;}
//    Q_INVOKABLE void removeProg(QString& path);
    Q_INVOKABLE void clear();
    int rowCount(QModelIndex const& parent = QModelIndex()) const override {return _progs.size();}
    QVariant data(const QModelIndex & index, int role) const override;
    bool setData(QModelIndex const&, QVariant const&, int role = Qt::EditRole) override;
    Qt::ItemFlags flags(QModelIndex const&) const override;

protected:
    QHash<int, QByteArray> roleNames() const override;

private:
    std::vector<Prog*> _progs {};
};

