/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/


#pragma once

#include <QWidget>
#include <QThread>
#include <QFileInfo>
#include <QString>
#include <QLabel>
#include <progmodel.h>

class ConversionWorker : public QObject {
    Q_OBJECT

private:
//    QFileInfoList serfiles;
    ProgModel* prog_model;
    QFileInfo curr_fileinfo;
    bool canceled;
    bool overwrite;
    bool to8bit;
    QString suffix;

public:
    explicit ConversionWorker(ProgModel* model, bool overwrite, bool to8bit, QString suffix);

public slots:
    void start();
    void cancel();

signals:
    void finished();

    void error_msg(const QString& text);
    void cancel_btn_text(const QString& text);
};

