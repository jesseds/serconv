#pragma once
#include <QObject>
#include <QFileInfo>
#include <QQuickView>
#include <conversionworker.h>
#include <entrymodel.h>
#include <progmodel.h>


// Document structure for QtQuick interface
class Document : public QObject {
    Q_OBJECT
    QML_SINGLETON
    QML_NAMED_ELEMENT(Document)

    Q_PROPERTY(bool subDirs READ subDirs WRITE setSubDirs NOTIFY subDirsChanged)
    Q_PROPERTY(bool castTo8Bit READ castTo8Bit WRITE setCastTo8Bit NOTIFY castTo8BitChanged)
    Q_PROPERTY(bool overwrite READ overwrite WRITE setOverwrite NOTIFY overwriteChanged)
    Q_PROPERTY(QString suffix READ suffix WRITE setSuffix NOTIFY suffixChanged)

    Q_PROPERTY(QAbstractListModel* entryModel READ entryModel CONSTANT)
    Q_PROPERTY(QAbstractListModel* progModel READ progModel CONSTANT)


public:
    explicit Document();

    void addEntry(Entry* entry);

    // ------------------ Properties ------------------
    void setSubDirs(bool val);
    auto subDirs() const {return _subdirs;}

    void setCastTo8Bit(bool val);
    auto castTo8Bit() const {return _cast_8bit;}

    void setOverwrite(bool val);
    auto overwrite() const {return _overwrite;}

    void setSuffix(QString& val);
    auto suffix() const {return _suffix;}

    auto entryModel() const {return _entry_model;}
    auto progModel() const {return _prog_model;}

    Q_INVOKABLE void addFromUrls(QList<QUrl>);
    Q_INVOKABLE void removeByAbsPath(QString);
    Q_INVOKABLE void convert();
    Q_INVOKABLE void clearEntries();
    Q_INVOKABLE void exitApp();
//    Q_INVOKABLE void applyBlur(QWindow* window);

private:
    EntryModel* _entry_model;
    ProgModel* _prog_model;

    // Properties
    bool _subdirs;
    bool _cast_8bit;
    bool _overwrite;
    QString _suffix = "";

    QThread* _convert_thread;
    ConversionWorker* _convert_worker;

public slots:
    void onConvertWorkerFinished();
    void cancel();// {_convert_worker->cancel();}

signals:
    void subDirsChanged(bool);
    void overwriteChanged(bool);
    void castTo8BitChanged(bool);
    void conversionStarted();
    void conversionFinished();
    void suffixChanged(QString);
};

