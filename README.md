![](https://gitlab.com/jesseds/serconv/-/raw/master/docs/imgs/file_folder.png)

# SERConv

SERConv is a Utility for easily converting series files from TIA (Emispec/ESVision)
microscopy software to the more versatile tiff format.

The purpose of this software is to provide a solution for batch conversion of
the TIA series (*.ser) to tiff format. Output from SERConv is primarily intended for
use in ImageJ and DigitalMicrograph, but should work for any application capable
of reading *.tiff files. SERConv supports conversion of basic 2d scans, such as
STEM data, while also supporting hyperspectral formats such as line scans and
spectrum images - important for getting TIA EELS into other software such as
DigitalMicrograph.

* Support for conversion of multidimensional data (spectra, imagery, and diffraction patterns).
* Direct support of hyperspectral EELS (spectrum imaging).
* Batch conversion.
* Support both 32- and 64-bit byte offsets that depend on TIA versions.
* Retains spatial calibration as tiff metadata
* Optional conversion to 8-bit depth.
* Optional recursive directory searching for ser files.
* Optional suffix to file names.

# Documentation
Documentation can be found at https://jesseds.gitlab.io/serconv/

# Download & Installation
1. An installer for Windows can be downloaded here: https://gitlab.com/jesseds/serconv/-/releases
2. Otherwise, the program may be compiled from source.

# Notes
* Spatial units are converted to nm, reciprocal unit calibration is not currently processed.

# Questions and Answers
**Doesn't program X or plugin Y do the same thing?**
: There are other programs, or extensions to programs, capable of converting TIA series files to  some degree. SERConv was originally made to handle 3 situations found lacking in other software: 

1. The conversion of spectrum images. Certain programs were unable to handle the higher-dimensional
nature of spectrum images properly. I.e. some programs will open each slice as separate images, or
not open at all.
2. A change in the data precision format stored in series files beginning from TIA version 4.7.3 is
not compatible with certain software.
3. Batch conversion of whole directories/subdirectories and/or specific combinations of series files,
similar to the GMS batch conversion feature for dm3 files.


**Why do converted images appear black or washed out when I view them?**
: Many generic image viewing programs only support 8-bit images. The data precision
saved in a *.ser file can be of multiple different data types and/or precisions.
This causes certain image viewing programs to display the image incorrectly. The
solution is to open/process the image in known programs such as ImageJ/GMS, or
to use the 8-bit conversion flag in the options pane of SERConv.


**Does SERConv not add a scalebar? How do I know the spatial calibration?**
: SERConv intentionally does not burn a scale bar into the image. The program is 
not intended to be used as an image processor, instead, the spatial calibration
is automatically embedded in the image in units of nanometers (reciprocal and energy units
are not embedded). It is up to the user to process the image how they desire.

# References
Details on the referenced series file format structure can be found below:
1. http://www.ntu.edu.sg/home/cbb/info/TIAformat/index.html
2. http://www.ntu.edu.sg/home/cbb/info/TIAformat/TIAseriesformat.pdf
   

