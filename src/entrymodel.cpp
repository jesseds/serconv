#include <entrymodel.h>
#include <QDirIterator>
#include <iostream>
#include <QTimer>
#include <QCoreApplication>

QFileInfoList accumulate_ser_paths(QDir& dir, bool subdirs) {
    if (!dir.exists()) {
        return QFileInfoList{};
    }

    QFileInfoList retn;

    QStringList lst;
    if (subdirs) {
        QDirIterator dit(dir.absolutePath(), lst << "*.ser", QDir::Files, QDirIterator::Subdirectories);
        auto next = dit.next();
        while (dit.hasNext()) {
            QFileInfo path(dit.next());
            retn.append(path);
        }
    }
    else {
        QDirIterator dit(dir.absolutePath(), lst << "*.ser", QDir::Files);
        while (dit.hasNext()) {
            QFileInfo pth(dit.next());
            retn.append(pth);
        }
    }
    return retn;
}


void Entry::setActive(bool val) {
    if (val == _active) {
        return;
    }

    _active = val;
}


void Entry::setSubDirs(bool val) {
    if (_subdirs == val) {
        return;
    }

    _subdirs = val;
    _updateSerList();
}


void Entry::_updateSerList() {
    if (_path.isDir()) {
        QDir dir {_path.absoluteFilePath()};
        _ser_files = accumulate_ser_paths(dir, _subdirs);
    }
}


Entry::Entry(QString file, bool subdirs) {
    if (QFileInfo(file) == _path) {
        return;
    }
    _subdirs = subdirs;
    _path = QFileInfo(file);
    _updateSerList();

//    if (_path.isDir()) {
//        QDir dir {_path.absoluteFilePath()};
//        _ser_files = accumulate_ser_paths(dir, false);
//    }
}


QString Entry::descr() const {
    if (isDir()) {
        return QString::fromStdString("Directory (" + std::to_string(numSerFiles()) + " ser files)");
    }
    else {
        return QString("File");
    }
}

QString Entry::name() const {
    if (isDir()) {
        return QString(_path.baseName() + "/*");
    }
    else {
        return _path.fileName();
    }
}


EntryModel::EntryModel(QObject* parent) : QAbstractListModel(parent) {}


void EntryModel::addEntry(Entry* entry) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _entries.push_back(entry);
    endInsertRows();
}


QVariant EntryModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= _entries.size())
        return QVariant();

    auto entry = _entries[index.row()];
    if (role == Path)
        return entry->path();
    if (role == Name)
        return entry->name();
    if (role == Active)
        return entry->active();
    if (role == Descr)
        return entry->descr();
    if (role == SubDirs)
        return entry->subDirs();

    return QVariant();
}


void EntryModel::removeEntry(QString& path) {
    for (int i=0; i<rowCount(); ++i) {
        if (_entries[i]->path() == path) {
            beginRemoveRows(QModelIndex(), i, i);
            _entries.erase(_entries.begin()+i);
            endRemoveRows();
        }
    }
}

void EntryModel::clear() {
    beginRemoveRows(QModelIndex(), 0, rowCount());
    _entries.clear();
    endRemoveRows();
}

void EntryModel::setSubDirs(bool val) {
    int rows = rowCount();
    for (int i=0; i<rows; ++i) {
        setData(index(i, 0), val, SubDirs);
    }
}

bool EntryModel::setData(QModelIndex const& index, QVariant const& value, int role) {
    auto entry = _entries[index.row()];

    if (role == Active) {
        if (entry->active() == value.toBool()) {
            return false;
        }

        _entries[index.row()]->setActive(value.toBool());
        emit dataChanged(index, index);
        return true;
    }
    if (role == SubDirs) {
        if (entry->subDirs() == value.toBool()) {
            return false;
        }

        _entries[index.row()]->setSubDirs(value.toBool());
        emit dataChanged(index, index);
        return true;

    }
    return false;
}


Qt::ItemFlags EntryModel::flags(QModelIndex const& index) const {
    if (not index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEditable;
}


QHash<int, QByteArray> EntryModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Path] = "path";
    roles[Name] = "name";
    roles[Descr]= "descr";
    roles[Active] = "active";
    roles[SubDirs] = "subDirs";
    return roles;
}
