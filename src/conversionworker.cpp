/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <conversionworker.h>
#include <QListIterator>
#include <output.h>
#include <QApplication>
#include <iostream>
#include <sstream>


ConversionWorker::ConversionWorker(ProgModel* model, bool overwrite, bool to8bit, QString suffix):
        QObject(),
        prog_model{model},
        canceled{false},
        overwrite{overwrite},
        to8bit{to8bit},
        suffix{suffix} {

}

void ConversionWorker::start() {
    QString label;
    int count {0};

    for (auto const& prog: prog_model->progs()) {
        auto ser = prog->path();
        QModelIndex index = prog_model->index(count, 0);

        if (canceled){
            prog_model->setData(index, Prog::Status::Canceled, ProgModel::Status);
            ++count;
            continue;
        }
        prog_model->setData(index, Prog::Status::Working, ProgModel::Status);

        QString ser_str(ser.absoluteFilePath());
        curr_fileinfo = convert_path_ext(ser);
        QString tiff_path = curr_fileinfo.absoluteFilePath();

        tiff_path.insert(tiff_path.length()-4, suffix);

        if (QFileInfo::exists(tiff_path) && !overwrite) {
            ++count;
            prog_model->setData(index, Prog::Status::Skipped, ProgModel::Status);
            continue;
        }

        if (! ser.exists()) {
            qDebug() << "Failed to convert SER does not exist: " << ser.absoluteFilePath();
            prog_model->setData(index, Prog::Status::Failed, ProgModel::Status);
            ++count;
            continue;
        }

        label.clear();
        std::stringstream txt {};
        QString pathstr = ser.absoluteFilePath();
        QFileInfo file = QFileInfo(pathstr);
        txt << file.baseName().toStdString() << ".";
        txt << file.suffix().toStdString();
        label = QString::fromStdString(txt.str());

        try {
            auto result = map_writer(ser_str, tiff_path, to8bit);
            if (result.has_value()) {
                qDebug() << "Failed to convert " << ser.absoluteFilePath() << result.value().data();
                prog_model->setData(index, Prog::Status::Failed, ProgModel::Status);
            }
            else {
                prog_model->setData(index, Prog::Status::Successful, ProgModel::Status);
            }
        }
        catch (std::exception e) {
            qDebug() << "Failed to convert " << ser.absoluteFilePath() << e.what();
            prog_model->setData(index, Prog::Status::Failed, ProgModel::Status);
        }
        ++count;
    }
    emit finished();
}


void ConversionWorker::cancel() {
    canceled = true;
}

