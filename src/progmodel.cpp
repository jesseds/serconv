#include <progmodel.h>
#include <QDirIterator>
#include <iostream>
#include <QTimer>
#include <QCoreApplication>



Prog::Prog(QFileInfo const& file) : _path(file) {}

void Prog::setResult(QString const& msg) {
    if (_result == msg) {
        return;
    }

    _result = msg;
}


void Prog::setStatus(Status stat) {
    if (_status == stat) {
        return;
    }

    _status = stat;
}


QString Prog::statusStr() const {
    switch (_status) {
    case Successful: {
        return "Successful";
    }
    case Queued: {
        return "Queued";
    }
    case Failed: {
        return "Failed";
    }
    case Skipped: {
        return "Skipped";
    }
    case Canceled: {
        return "Canceled";
    }
    case Working: {
        return "Working...";
    }
    default: {
        qDebug() << "Unknown status str";
        return "";
    }
    }
}


ProgModel::ProgModel(QObject* parent) : QAbstractListModel(parent) {}


void ProgModel::addProg(Prog* prog) {
    beginInsertRows(QModelIndex(), rowCount(), rowCount());
    _progs.push_back(prog);
    endInsertRows();
}


QVariant ProgModel::data(const QModelIndex & index, int role) const {
    if (index.row() < 0 || index.row() >= _progs.size())
        return QVariant();

    auto prog = _progs[index.row()];
    if (role == Path)
        return prog->path().dir().dirName() + "/" + prog->path().fileName();
    if (role == Status)
        return prog->statusStr();
//    if (role == Active)
//        return prog->active();
//    if (role == Descr)
//        return prog->descr();

    return QVariant();
}


//void ProgModel::removeProg(QString& path) {
//    for (int i=0; i<rowCount(); ++i) {
//        if (_entries[i]->path() == path) {
//            beginRemoveRows(QModelIndex(), i, i);
//            _entries.erase(_entries.begin()+i);
//            endRemoveRows();
//        }
//    }
//}

void ProgModel::clear() {
    beginRemoveRows(QModelIndex(), 0, rowCount());
    _progs.clear();
    endRemoveRows();
}


bool ProgModel::setData(QModelIndex const& index, QVariant const& value, int role) {
    auto prog = _progs[index.row()];

    if (role == Status) {
        if (prog->status() == value.value<Prog::Status>()) {
            return false;
        }
        else {
            prog->setStatus(value.value<Prog::Status>());
            emit dataChanged(index, index);
            return true;
        }
    }
//    if (role == Active) {
//        if (prog->active() == value.toBool()) {
//            return false;
//        }

//        _entries[index.row()]->setActive(value.toBool());
//        emit dataChanged(index, index);
//        return true;
//    }
    return false;
}


Qt::ItemFlags ProgModel::flags(QModelIndex const& index) const {
    if (not index.isValid()) {
        return Qt::NoItemFlags;
    }

    return Qt::ItemIsEditable;
}


QHash<int, QByteArray> ProgModel::roleNames() const {
    QHash<int, QByteArray> roles;
    roles[Path] = "path";
    roles[Status] = "status";
    return roles;
}
