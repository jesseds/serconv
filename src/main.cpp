/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <QApplication>
//#include <mainwindow.h>
#include <updatechecker.h>
#include <document.h>
#include <QQmlApplicationEngine>
#include <iostream>
#include <QQmlContext>
#include <QIcon>
#include <QQuickStyle>
#include <entrymodel.h>


int main(int argc, char **argv) {
    QCoreApplication::setOrganizationName("Jesse Smith");
    QCoreApplication::setOrganizationDomain("Jesse Smith");
    QCoreApplication::setAttribute(Qt::AA_UseOpenGLES);
    QApplication app (argc, argv);

    app.setWindowIcon(QIcon(":SERConv/icons/serconv.ico"));
//    app.setWindowIcon(QIcon(":SERConv/icons/logo2.svg"));
    QApplication::setApplicationVersion(VERSION);

    // Check for updates
    UpdateChecker* updater = new UpdateChecker();
    updater->show_no_update_message = false;
    updater->run();

    QQmlApplicationEngine engine;

    const QUrl url("qrc:SERConv/ui/main.qml");

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
    if (!obj && url == objUrl)
        QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    QQmlContext* context = engine.rootContext();
    context->setContextProperty("VERSION", VERSION);
    engine.load(url);

    return app.exec();
}





