﻿#include <document.h>
#include <QQmlContext>
#include <QDir>
#include <iostream>
#include <QQmlEngine>
#include <QDirIterator>
#include <QApplication>
#include <QGraphicsDropShadowEffect>


void Document::exitApp() {
    QApplication::exit();
}


void Document::addEntry(Entry* entry) {
    _entry_model->addEntry(entry);
    qDebug() << "Added entry " << entry->name();
}

//void Document::applyBlur(QWindow *window){
//    auto effect = new QGraphicsDropShadowEffect();
//    effect->setBlurRadius(5);
//    effect->setXOffset(5);
//    effect->setYOffset(5);
//    effect->setColor(QColor("#ff0000"));
//    window->setGraphicsEffect(effect);
//}


void Document::addFromUrls(QList<QUrl> urls) {
    for (int i=0; i<urls.size();++i) {
        if (not urls[i].isValid()) {
            qDebug() << "not valid: " << urls[i];
            continue;
        }
        QFileInfo local {urls[i].toLocalFile()};
        if (local.exists()) {
            auto entry = new Entry{local.absoluteFilePath(), subDirs()};
            addEntry(entry);
        }
    }
}

void Document::cancel() {
    _convert_worker->cancel();
}

void Document::setSuffix(QString& val) {
    if (val == _suffix) {
        return;
    }

    _suffix = val;
    emit suffixChanged(val);
}


Document::Document() : QObject() {
    _entry_model = new EntryModel(this);
    _prog_model = new ProgModel(this);

    setSubDirs(true);
    setOverwrite(false);
    setCastTo8Bit(false);

    _convert_thread = new QThread(this);
}


void Document::clearEntries() {
    _entry_model->clear();
}


void Document::convert() {
    emit conversionStarted();

    QFileInfoList ser_files {};
    _prog_model->clear();


    for (auto const& obj: _entry_model->entries()) {
        auto entry = dynamic_cast<Entry*>(obj);

        // Should never happen
        if (entry == nullptr) {
            qDebug() << "Could not cast object to Entry*";
            continue;
        }

        // Directories
        if (entry->isDir()) {
            QDir dir = QDir(entry->path());
            QFileInfoList paths = accumulate_ser_paths(dir, subDirs());
            for (auto const& path: paths) {
                ser_files.append(path);
            }
        }
        // Single files
        else {
            ser_files.append(QFileInfo(entry->path()));
        }
    }

    for (auto const& ser: ser_files) {
        _prog_model->addProg(new Prog(ser));
    }

    _convert_worker = new ConversionWorker(_prog_model, overwrite(), castTo8Bit(), suffix());
    _convert_worker->moveToThread(_convert_thread);
    connect(_convert_thread, &QThread::started, _convert_worker, &ConversionWorker::start);
    connect(_convert_thread, &QThread::finished, _convert_worker, &ConversionWorker::deleteLater);
    connect(_convert_worker, &ConversionWorker::finished, this, &Document::onConvertWorkerFinished);
    _convert_thread->start();
}


void Document::onConvertWorkerFinished() {
    _convert_thread->quit();
    _convert_thread->wait();
    emit conversionFinished();
}


void Document::removeByAbsPath(QString remove_path) {
    _entry_model->removeEntry(remove_path);
}

void Document::setSubDirs(bool val) {
    if (val == _subdirs) {
        return;
    }

    _subdirs = val;

    _entry_model->setSubDirs(val);
    emit subDirsChanged(val);
}

void Document::setOverwrite(bool val) {
    if (val == _overwrite) {
        return;
    }

    _overwrite = val;

    emit overwriteChanged(val);
}

void Document::setCastTo8Bit(bool val) {
    if (val == _cast_8bit) {
        return;
    }

    _cast_8bit = val;

    emit castTo8BitChanged(val);
}
