/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <aboutdialog.h>

#include <QVBoxLayout>
#include <QFile>
#include <QTextStream>
#include <tiffio.h>

AboutDialog::AboutDialog(QWidget* parent) : QDialog(parent) {
    this->setWindowTitle("About SERConv");
    this->resize(500, 300);

    toplayout = new QVBoxLayout();
    this->setLayout(toplayout);

    tabs = new QTabWidget(this);
    toplayout->addWidget(tabs);
    about_widget = new QWidget(this);
    aboutlayout = new QVBoxLayout();
    about_widget->setLayout(aboutlayout);

    tabs->addTab(about_widget, "About");

    serconv_lbl = new QLabel("<b><font size=12>SERConv</font></b>");
    aboutlayout->addWidget(serconv_lbl);
    aboutlayout->setAlignment(serconv_lbl, Qt::AlignHCenter);
    QLabel* version_lbl = new QLabel("<b>SERConv version: </b>" + QString::fromStdString(VERSION) + '\n');
    aboutlayout->addWidget(version_lbl);
    aboutlayout->setAlignment(version_lbl, Qt::AlignHCenter);

    QLabel* qt_vers_label = new QLabel("<b>Qt version: </b>" + QString::fromStdString(QT_VERSION_STR));
    aboutlayout->addWidget(qt_vers_label);
    aboutlayout->setAlignment(qt_vers_label, Qt::AlignHCenter);


    std::string tiff_ver = TIFFGetVersion();
    QLabel* tiff_vers_label = new QLabel("<b>libtiff version:</b> " + QString::fromStdString(tiff_ver));
    aboutlayout->addWidget(tiff_vers_label);
    aboutlayout->setAlignment(tiff_vers_label, Qt::AlignHCenter);

    link = new QLabel("Website: <a href=\"https://gitlab.com/jesseds/serconc\">https://gitlab.com/jesseds/serconv</a>");
    link->setTextInteractionFlags(Qt::TextBrowserInteraction);
    link->setTextFormat(Qt::RichText);
    link->setOpenExternalLinks(true);
    aboutlayout->addWidget(link);
    aboutlayout->setAlignment(link, Qt::AlignHCenter);
    copyright = new QLabel("Copyright 2022 Jesse Smith\n");
    aboutlayout->addWidget(copyright);
    aboutlayout->setAlignment(copyright, Qt::AlignHCenter);

    description = new QLabel("SERConv is a utility for easily converting series files from\nTIA (Emispec/ESVision) microscopy software to the more versatile tiff format.");

    aboutlayout->addWidget(description);
    aboutlayout->setAlignment(description, Qt::AlignHCenter);
    aboutlayout->addStretch();

    QFile license_file(":/LICENSE");
    license_file.open(QFile::ReadOnly|QFile::Text);
    QTextStream stream(&license_file);

    license = new QTextEdit(this);
    license->setReadOnly(true);
    license->setText(stream.readAll());

    tabs->addTab(license, "License");

    btns = new QDialogButtonBox(QDialogButtonBox::Ok, this);
    toplayout->addWidget(btns);
    connect(btns, &QDialogButtonBox::accepted, this, &AboutDialog::close);

}
