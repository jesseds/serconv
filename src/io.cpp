/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <io.h>
#include <vector>
#include <string>
#include <QDebug>
#include <algorithm>
#include <QString>
#include <iostream>
#include <ciso646>


size_t DataND::DataTypeSize() {
    switch (this->DataType) {
    case 1: {
        return 1;
    }
    case 2: {
        return 2;
    }
    case 3: {
        return 4;
    }
    case 4: {
        return 1;
    }
    case 5: {
        return 2;
    }
    case 6: {
        return 4;
    }
    case 7: {
        return 4;
    }
    case 8: {
        return 8;
    }
    default: {
        throw std::invalid_argument("Invalid data type ID");
    }
    }
}


template <class T>
T read_single(std::ifstream& file) {
    if (std::is_trivially_copyable<T>::value) {
        T retn;
        file.read(reinterpret_cast<char *>(&retn), sizeof(T));
        return retn;
    }
    else {
        throw(std::invalid_argument("Template parameter is not trivially copiable"));
    }
}


template <class T>
std::vector<double> read_sequence(std::ifstream& ifile, uint& nelem) {
    std::vector<T> temp(nelem);
    std::vector<double> retn;
    ifile.read(reinterpret_cast<char *>(temp.data()), nelem*sizeof(T));
    retn = std::vector<double>(temp.begin(), temp.end());
    return retn;
}


std::string read_string(std::ifstream& ifile, uint& size) {
    std::string retn;
    for (uint j=0; j<size; ++j) {
        retn += read_single<char>(ifile);
    }
    return retn;
}

template <class T>
std::pair<double, double> find_min_max(std::vector<T>& data) {
    auto minmax = std::minmax_element(data[0].Data.begin(), data[0].Data.end());
    auto min = *minmax.first;
    auto max = *minmax.second;

    auto dat_it = data.begin();
    ++dat_it;

    for (auto& it = dat_it; it<data.end(); ++it) {
        auto minmax = std::minmax_element((*it).Data.begin(), (*it).Data.end());
        if (*minmax.first < min) {
            min = *minmax.first;
        }
        if (*minmax.second > max) {
            max = *minmax.second;
        }
    }

    std::pair<double, double> retn(min, max);
    return retn;
}


std::vector<double> read_array(std::ifstream& ifile, uint& nelem, int dtype) {
    std::vector<double> datum;

    switch (dtype) {
    case 1: {
        datum = read_sequence<uint8_t>(ifile, nelem);
        break;
    }
    case 2: {
        datum = read_sequence<uint16_t>(ifile, nelem);
        break;
    }
    case 3: {
        datum = read_sequence<uint32_t>(ifile, nelem);
        break;
    }
    case 4: {
        datum = read_sequence<int8_t>(ifile, nelem);
        break;
    }
    case 5: {
        datum = read_sequence<int16_t>(ifile, nelem);
        break;
    }
    case 6: {
        datum = read_sequence<int32_t>(ifile, nelem);
        break;
    }
    case 7: {
        datum = read_sequence<float>(ifile, nelem);
        break;
    }
    case 8: {
        datum = read_sequence<double>(ifile, nelem);
        break;
    }
    default: {
        throw std::invalid_argument("Invalid data type ID");
    }
    }
    return datum;
}


bool validate_header(SerData& ser) {
    if (ser.ByteOrder != 0x4949) {
        return false;
    }
    if ((ser.SeriesVersion != 0x0220) && (ser.SeriesVersion != 0x0210)) {
        return false;
    }
    if (ser.SeriesID != 0x0197) {
        return false;
    }
    if ((ser.DataTypeID != 0x4120) && (ser.DataTypeID != 0x4122)) {
        return false;
    }
    if ((ser.TagTypeID != 0x4152) && (ser.TagTypeID != 0x4142)) {
        return false;
    }
    return true;
}


SerData read_ser(std::string& path) {
    std::ifstream ifile(path, std::ios::binary);
    SerData ser;
    if (ifile.is_open()) {

        // Header
        ser.ByteOrder = read_single<int16_t>(ifile);
        if (ser.ByteOrder != 0x4949) {
            throw std::runtime_error("Invalid ser file");
        }
        ser.SeriesID = read_single<int16_t>(ifile);
        ser.SeriesVersion = read_single<int16_t>(ifile);
        ser.DataTypeID = read_single<int32_t>(ifile);
        ser.TagTypeID = read_single<int32_t>(ifile);

        if (!validate_header(ser)) {
            throw std::runtime_error("SER header is invalid");
        }

        ser.TotalNumberElements = read_single<int32_t>(ifile);
        ser.ValidNumberElements = read_single<int32_t>(ifile);

        bool use_8byte {ser.SeriesVersion == 0x0220};
        ser.OffsetArrayOffset = use_8byte ? read_single<int64_t>(ifile) : read_single<int32_t>(ifile);
        ser.NumberDimensions = read_single<uint32_t>(ifile);

        int headersize = use_8byte ? 34 : 30;

        // Dimension arrays
        ifile.seekg(headersize);
        for (uint i=0; i < ser.NumberDimensions; ++i) {
            DimensionArray dim;
            dim.DimensionSize = read_single<uint32_t>(ifile);
            dim.CalibrationOffset = read_single<double>(ifile);
            dim.CalibrationDelta = read_single<double>(ifile);
            dim.CalibrationElement = read_single<int32_t>(ifile);
            dim.DescriptionLength = read_single<uint32_t>(ifile);

            dim.Description = read_string(ifile, dim.DescriptionLength);

            dim.UnitsLength = read_single<uint32_t>(ifile);
            dim.Units = read_string(ifile, dim.UnitsLength);

            if (ser.spatial_unit == "" and dim.Units != "") {
                ser.spatial_unit = dim.Units;
            }
            if (ser.spatial_unit == "" and dim.Units == "") {
                ser.spatial_unit = "meters";
            }

            ser.Dimensions.push_back(std::move(dim));
        }

        // Data and Tag offset arrays
        ifile.seekg(ser.OffsetArrayOffset);
        int64_t val;
        for (auto i=0; i<ser.TotalNumberElements; ++i) {
            val = use_8byte ? read_single<int64_t>(ifile) : read_single<int32_t>(ifile);
            ser.DataOffsetArray.push_back(val);
        }

        for (auto i=0; i<ser.TotalNumberElements; ++i) {
            val = use_8byte ? read_single<int64_t>(ifile) : read_single<int32_t>(ifile);
            ser.TagOffsetArray.push_back(val);
        }

        // Actual data
        switch(ser.DataTypeID) {
        case 0x4120: {
            // 1D data
            for (auto& i: ser.DataOffsetArray) {
                ifile.seekg(i);
                Data1D data1d;
                data1d.CalibrationOffset = read_single<double>(ifile);
                data1d.CalibrationDelta = read_single<double>(ifile);
                data1d.CalibrationElement = read_single<uint32_t>(ifile);
                data1d.DataType = read_single<uint16_t>(ifile);
                data1d.ArrayLength = read_single<uint32_t>(ifile);
                unsigned int nelem = data1d.ArrayLength;

                data1d.Data.resize(data1d.ArrayLength);

                data1d.Data = read_array(ifile, nelem, data1d.DataType);
                ser.Data_1D.push_back(std::move(data1d));
            }
        }
            break;
        case 0x4122: {
            // 2D data
            if (ser.Data_1D.size() > 0) {
                throw std::runtime_error("Unable to interpret ser file, does not appear to be a 2d image, 1d spectrum, linescan, or spectrum image.");
            }
            for (auto& i: ser.DataOffsetArray) {
                ifile.seekg(i);

                Data2D data2d;
                data2d.CalibrationOffsetX = read_single<double>(ifile);
                data2d.CalibrationDeltaX = read_single<double>(ifile);
                data2d.CalibrationElementX = read_single<int32_t>(ifile);
                data2d.CalibrationOffsetY = read_single<double>(ifile);
                data2d.CalibrationDeltaY = read_single<double>(ifile);
                data2d.CalibrationElementY = read_single<int32_t>(ifile);
                data2d.DataType = read_single<uint16_t>(ifile);
                data2d.ArraySizeX = read_single<uint32_t>(ifile);
                data2d.ArraySizeY = read_single<uint32_t>(ifile);

                unsigned int nelem = data2d.ArraySizeX*data2d.ArraySizeY;
                data2d.Data.resize(nelem);

                data2d.Data = read_array(ifile, nelem, data2d.DataType);
                ser.Data_2D.push_back(std::move(data2d));
            }
            break;
        }
        }
        ifile.close();
    }
    else {
        throw std::invalid_argument("Unable to open file");
    }
    return ser;
}


SerData read_ser(QString& fpath) {
    std::string pth = fpath.toStdString();
    SerData retn = read_ser(pth);
    return retn;
}

