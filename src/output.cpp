/*******************************************************************************
** This file is part of SERConv.
**
** SERConv is a utility for easily converting series files from
** TIA (Emispec/ESVision) microscopy software to the more versatile tiff format.
**
** Copyright (C) 2019 Jesse Smith
**
** SERConv is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 2 of the License, or
** (at your option) any later version.
**
** SERConv is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with SERConv.  If not, see <http://www.gnu.org/licenses/>.
*******************************************************************************/

#include <output.h>

#include <vector>
#include <QFileInfo>
#include <QString>
#include <QDir>
#include <QDirIterator>
#include <tiffio.h>
#include <io.h>
#include <algorithm>
#include <sstream>
#include <iostream>


using std::vector;


template <class T>
void write_1d_data(TIFF* tif, vector<double>& indata, uint row=0) {
    vector<T> datbuf = vector<T>(indata.begin(), indata.end());
    const T* buf = datbuf.data();
    int result = TIFFWriteScanline(tif, (void *)(buf), row, 0);
    if (result == -1) {
        throw std::runtime_error("Write failed");
    }
}


template <class T>
void write_2d_data(TIFF* tif, vector<double>& indata, uint rows, uint cols, bool normalize=false) {

    if (normalize) {
        auto minmax = std::minmax_element(indata.begin(), indata.end());
        auto min = *minmax.first;
        auto max = *minmax.second;

        for (auto& elem: indata) {
            elem = 255*(elem-min)/(max-min);
        }
    }
    vector<T> datbuf = vector<T>(indata.begin(), indata.end());
    const T* buf = datbuf.data();

    for (uint i=0; i<rows; ++i) {
        int result = TIFFWriteScanline(tif, (void *)(buf+((rows-i-1)*cols)), i, 0);
        if (result == -1) {
            throw std::runtime_error("Write failed");
        }
    }
}


template <class T>
void write_2d_page_data(TIFF* tif, vector<Data1D>& indata, uint& page, uint& lengthx, uint& lengthy) {
    vector<T> pagedata;
    pagedata.resize(lengthx*lengthy);

    for (uint pg=0; pg< indata.size(); ++pg) {
        pagedata[pg] = T(indata[pg].Data[page]);
    }

    for (uint row=0; row<lengthy; ++row) {
        int result = TIFFWriteScanline(tif, (void *)(pagedata.data()+((row)*lengthx)), row, 0);
        if (result == -1) {
            throw std::runtime_error("Write failed");
        }
    }
}


void write_spectrum_image(std::string& fpath, vector<DimensionArray>& dims, vector<Data1D>& indata, std::string unit) {
    if (indata.size() < 4) {
        throw std::invalid_argument("Too few Data1D objects than expected");
    }
    if (dims.size() != 2) {
        throw std::invalid_argument("Expected exactly two dimensions");
    }

    TIFF* tif = TIFFOpen(fpath.c_str(), "w");
    if (!tif) {
        throw std::runtime_error("Could not open file for writing");
    }
    size_t dtype_size = {8*indata[0].DataTypeSize()};

    auto lengthx = dims[0].DimensionSize;
    auto lengthy = dims[1].DimensionSize;
    auto npages = indata[0].Data.size();
    double xres = abs(1/(1e9*dims[0].CalibrationDelta));
    double yres = abs(1/(1e9*dims[1].CalibrationDelta));

    std::string descr = image_description(lengthx, lengthy, npages, unit);

    for (uint page=0; page<npages; ++page) {
        TIFFSetField(tif, TIFFTAG_XRESOLUTION, xres);
        TIFFSetField(tif, TIFFTAG_YRESOLUTION, yres);
        TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, lengthx);
        TIFFSetField(tif, TIFFTAG_IMAGELENGTH, lengthy);
        TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
        TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, dtype_size);
        TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, 1);
        TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, descr.data());
        TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, lengthx);
        TIFFSetField(tif, TIFFTAG_PAGENUMBER, page, npages);
        TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, 1);

        TIFFSetField(tif, TIFFTAG_SUBFILETYPE, 0);
        TIFFSetField(tif, 284, 1);

        switch (indata[0].DataType) {
        case 1: {
            write_2d_page_data<uint8_t>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 2: {
            write_2d_page_data<uint16_t>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 3: {
            TIFFSetField(tif, 339, 1);
            write_2d_page_data<uint32_t>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 4: {
            write_2d_page_data<int8_t>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 5: {
            write_2d_page_data<int16_t>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 6: {
            TIFFSetField(tif, 339, 2);
            write_2d_page_data<int32_t>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 7: {
            TIFFSetField(tif, 339, 3);
            write_2d_page_data<float>(tif, indata, page, lengthx, lengthy);
            break;
        }
        case 8: {
            write_2d_page_data<double>(tif, indata, page, lengthx, lengthy);
            break;
        }
        }
        TIFFWriteDirectory(tif);
    }

    TIFFClose(tif);
}


void write_linescan(std::string& fpath, vector<Data1D>& indata, std::string unit) {
    if (indata.size() < 1) {
        throw std::invalid_argument("Expected more than 1 1D data");
    }

    TIFF* tif = TIFFOpen(fpath.c_str(), "w");
    if (!tif) {
        throw std::runtime_error("Could not open file for writing");
    }
    size_t dtype_size {8*indata[0].DataTypeSize()};

    auto cols = indata.size();
    auto rows = indata[0].ArrayLength;

    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, rows);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, cols);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, dtype_size);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);

    for (uint i=0; i<indata.size(); ++i) {
        switch (indata[i].DataType) {
        case 1: {
            write_1d_data<uint8_t>(tif, indata[i].Data, i);
            break;
        }
        case 2: {
            write_1d_data<uint16_t>(tif, indata[i].Data, i);
            break;
        }
        case 3: {
            write_1d_data<uint32_t>(tif, indata[i].Data, i);
            break;
        }
        case 4: {
            write_1d_data<int8_t>(tif, indata[i].Data, i);
            break;
        }
        case 5: {
            write_1d_data<int16_t>(tif, indata[i].Data, i);
            break;
        }
        case 6: {
            write_1d_data<int32_t>(tif, indata[i].Data, i);
            break;
        }
        case 7: {
            write_1d_data<float>(tif, indata[i].Data, i);
            break;
        }
        case 8: {
            write_1d_data<double>(tif, indata[i].Data, i);
            break;
        }
        }
    }
    TIFFClose(tif);
}


void write_spectrum(std::string& fpath, Data1D& indata, std::string unit) {
    TIFF* tif = TIFFOpen(fpath.c_str(), "w");
    if (!tif) {
        throw std::runtime_error("Could not open file for writing");
    }
    size_t dtype_size = {8*indata.DataTypeSize()};

    auto nelem = indata.ArrayLength;

    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, nelem);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, 1);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, dtype_size);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);

    switch (indata.DataType) {
    case 1: {
        write_1d_data<uint8_t>(tif, indata.Data);
        break;
    }
    case 2: {
        write_1d_data<uint16_t>(tif, indata.Data);
        break;
    }
    case 3: {
        write_1d_data<uint32_t>(tif, indata.Data);
        break;
    }
    case 4: {
        write_1d_data<int8_t>(tif, indata.Data);
        break;
    }
    case 5: {
        write_1d_data<int16_t>(tif, indata.Data);
        break;
    }
    case 6: {
        write_1d_data<int32_t>(tif, indata.Data);
        break;
    }
    case 7: {
        write_1d_data<float>(tif, indata.Data);
        break;
    }
    case 8: {
        write_1d_data<double>(tif, indata.Data);
        break;
    }
    }
    TIFFClose(tif);
}

std::string image_description(int x, int y, int pages, std::string unit) {
    std::stringstream ss {};
    ss << "ImageJ=1.53f";
//    ss << "\nimages=" << pages;
//    ss << "\nslices=" << pages;
    ss << "\nunit=" << unit;
    ss << "\nloop=false";
//    ss << "\nchannels=1";
//    ss << "\nframes=" << pages;

    return ss.str();
}

void write_image(std::string& fpath, Data2D& indata, bool to8bit, std::string unit) {
    TIFF* tif = TIFFOpen(fpath.c_str(), "w");
    if (!tif) {
        throw std::runtime_error("Could not open file for writing");
    }

    size_t dtype_size = to8bit ? 8 : 8*indata.DataTypeSize();

    auto cols = indata.ArraySizeY;
    auto rows = indata.ArraySizeX;
    TIFFSetField(tif, 254, 0);
    TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, rows);
    TIFFSetField(tif, TIFFTAG_IMAGELENGTH, cols);
    TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1);
    TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, dtype_size);
    TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE);
    TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, 1);
    TIFFSetField(tif, TIFFTAG_PHOTOMETRIC, 1);
    double xres = 1/(1e9*indata.CalibrationDeltaX);
    double yres = 1/(1e9*indata.CalibrationDeltaY);
    TIFFSetField(tif, TIFFTAG_XRESOLUTION, xres);
    TIFFSetField(tif, TIFFTAG_YRESOLUTION, yres);
    TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, image_description(cols, rows, 1, unit).data());

    if (to8bit) {
        write_2d_data<uint8_t>(tif, indata.Data, rows, cols, true);
    }
    else {
        switch (indata.DataType){
        case 1: {
            write_2d_data<uint8_t>(tif, indata.Data, rows, cols);
            break;
        }
        case 2: {
            write_2d_data<uint16_t>(tif, indata.Data, rows, cols);
            break;
        }
        case 3: {
            write_2d_data<uint32_t>(tif, indata.Data, rows, cols);
            break;
        }
        case 4: {
            write_2d_data<int8_t>(tif, indata.Data, rows, cols);
            break;
        }
        case 5: {
            write_2d_data<int16_t>(tif, indata.Data, rows, cols);
            break;
        }
        case 6: {
            write_2d_data<int32_t>(tif, indata.Data, rows, cols);
            break;
        }
        case 7: {
            write_2d_data<float>(tif, indata.Data, rows, cols);
            break;
        }
        case 8: {
            write_2d_data<double>(tif, indata.Data, rows, cols);
            break;
        }
        }
    }
    TIFFClose(tif);
}


std::optional<std::string> map_writer(string& serpath, string& outpath, bool to8bit) {
    SerData ser;
    std::optional<std::string> retn;
    try {
        ser = read_ser(serpath);
    }
    catch (std::exception& e) {
        retn = e.what();
        return retn;
    }

    uint ndims {ser.NumberDimensions};
    size_t n2d {ser.Data_2D.size()};
    size_t n1d {ser.Data_1D.size()};

    bool islength = ser.spatial_unit == "meters";
    auto unit = islength ? "nm" : ser.spatial_unit;

    if ((n1d == 1) && (n2d == 0) && (ndims == 1)) {
        write_spectrum(outpath, ser.Data_1D[0], unit);
    }
    else if ((n1d == 0) && (n2d == 1) && (ndims == 1)) {
        write_image(outpath, ser.Data_2D[0], to8bit, unit);
    }
    else if ((n1d > 1) && (n2d == 0) && (ndims == 2)) {
        write_spectrum_image(outpath, ser.Dimensions, ser.Data_1D, unit);
    }
    else if ((n1d > 1) && (n2d == 0) && (ndims == 1)) {
        write_linescan(outpath, ser.Data_1D, unit);
    }
    else{
        throw std::invalid_argument("Unable to determine the correct writer.");
    }
    return retn;
}


std::optional<std::string> map_writer(QString& serpath, QString& outpath, bool to8bit, QString const& suffix) {
    outpath.insert(outpath.length()-4, suffix);
    std::string out = outpath.toStdString();
    std::string ser = serpath.toStdString();
    return map_writer(ser, out, to8bit);
}


QFileInfo convert_path_ext(QFileInfo const& fpath) {
    QString outpath = fpath.absoluteDir().filePath(fpath.completeBaseName() + ".tif");
    return QFileInfo(outpath);
}
