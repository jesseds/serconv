import QtQuick
import QtQuick.Controls.Material

ToolButton {
    id: control
    SerStyle {id: style}
    signal clicked()
//    property alias implicitWidth: rect.implicitWidth
//    property alias implicitHeight: rect.implicitHeight
    property alias radius: rect.radius
    leftPadding: 10
    rightPadding: 10

    onClicked: focus = true

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true
//        preventStealing: true
        onClicked: parent.clicked()
        onEntered: {
            if (control.enabled)
                rect.opacity = 0.5
        }
        onExited: rect.opacity  = 0
        onPressed: rect.opacity = 1
        onReleased: rect.opacity = 0
    }

    background: Rectangle {
        id: rect
        anchors.fill: parent
//        color: Qt.lighter(Material.backgroundColor, 1.5)
        color: style.backgroundDimColor2
        opacity: 0
    }
}

