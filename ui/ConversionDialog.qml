import QtQuick
import QtQuick.Layouts
import QtQuick.Controls.Material
import QtQuick.Controls
import SERConv

Dialog {
    id: root
    property string titleBase: "Progress: "
    parent: Overlay.overlay
    width: 700
    height: 400
    dim: false
    anchors.centerIn: parent
    modal: true
    closePolicy: Dialog.NoAutoClose

    Connections {
        target: Document
        function onConversionStarted() {
            cancelBtn.enabled = true
            closeBtn.enabled = false
            title = titleBase + "converting..."
        }

        function onConversionFinished() {
            cancelBtn.enabled = false
            closeBtn.enabled = true
            title = titleBase + "Completed"
        }
    }

    Component {
        id: delegate
        Item {
            height: 24
            width: list.width

            RowLayout {
                anchors.fill: parent
                Label {text: path}
                Item {width: 3}
                Rectangle {height: 1; Layout.fillWidth: true; color: Material.foreground; opacity: 0.1}
                Item {width: 3}
                Label {text: status}
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        Rectangle {
            Layout.fillHeight: true
            Layout.fillWidth: true
            border.width: 1
            border.color: Material.accentColor
            radius: 6
            color: Qt.darker(Material.backgroundColor, 1.2)

            ListView {
                id: list
                clip: true
                model: Document.progModel
                anchors.fill: parent
                anchors.margins: 6
                anchors.topMargin: 2
                spacing: 0

                delegate: delegate
                onCountChanged: {if (list.count !== 0) {noFilesText.visible = false} else {noFilesText.visible = true}}
            }
            Label {id: noFilesText
                text: "No files"
                anchors.centerIn: parent
                font.pointSize: 14
                opacity: 0.375
            }
        }

        DialogButtonBox {
            Layout.fillWidth: true
            Button {
            id: cancelBtn
            text: "Stop"
            flat: true

            onClicked: {
                title = titleBase + "stopping after current file finished..."
                Document.cancel()
            }
        }
        Button {
            id: closeBtn
            text: "Close"
            flat: true
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
        onRejected: close()
    }
}
}
