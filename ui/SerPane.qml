import QtQuick
import QtQuick.Controls.Material
import QtQuick.Controls.Material.impl

Pane {
    id: control
    property int radius: 8
    property alias border: bkg.border

    SerStyle {id: style}

    background: Rectangle {
        id: bkg
//        color: Material.theme === Material.Light ? Qt.darker(Material.backgroundColor, 1.035) : Qt.lighter(Material.backgroundColor, 1.2)
        color: style.backgroundDimColor1
        radius: control.Material.elevation > 0 ? control.radius : 0

        layer.enabled: control.enabled && control.Material.elevation > 0
        layer.effect: ElevationEffect {
            elevation: control.Material.elevation
        }
    }
}
