import QtQuick
import QtQuick.Controls.Material

Item {
    id: root
    property int borderWidth: 5
    property ApplicationWindow appWindow

    MouseArea {
        id: left
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter
        width: borderWidth
        height: root.height - 2*borderWidth
        hoverEnabled: true
        cursorShape: Qt.SizeHorCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.LeftEdge)}
        }
    }

    MouseArea {
        id: right
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        width: borderWidth
        height: root.height - 2*borderWidth
        hoverEnabled: true
        cursorShape: Qt.SizeHorCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.RightEdge)}
        }
    }

    MouseArea {
        id: bottom
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        width: root.width - 2*borderWidth
        height: borderWidth
        hoverEnabled: true

        cursorShape: Qt.SizeVerCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.BottomEdge)}
        }
    }

    MouseArea {
        id: top
        anchors.top: parent.top
        anchors.horizontalCenter: parent.horizontalCenter
        width: root.width - 2*borderWidth
        height: borderWidth
        hoverEnabled: true
        cursorShape: Qt.SizeVerCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.TopEdge)}
        }
    }
    MouseArea {
        id: bottomLeft
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        width: borderWidth
        height: borderWidth
        hoverEnabled: true

        cursorShape: Qt.SizeBDiagCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.BottomEdge | Qt.LeftEdge)}
        }
    }

    MouseArea {
        id: topLeft
        anchors.top: parent.top
        anchors.left: parent.left
        width: borderWidth
        height: borderWidth
        hoverEnabled: true

        cursorShape: Qt.SizeFDiagCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.TopEdge | Qt.LeftEdge)}
        }
    }

    MouseArea {
        id: topRight
        anchors.top: parent.top
        anchors.right: parent.right
        width: borderWidth
        height: borderWidth
        hoverEnabled: true

        cursorShape: Qt.SizeBDiagCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.TopEdge | Qt.RightEdge)}
        }
    }

    MouseArea {
        id: bottomRight
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        width: borderWidth
        height: borderWidth
        hoverEnabled: true

        cursorShape: Qt.SizeFDiagCursor;
        acceptedButtons: Qt.NoButton

        DragHandler {
            grabPermissions: TapHandler.CanTakeOverFromAnything
            target: null
            onActiveChanged: if (active) {appWindow.startSystemResize(Qt.BottomEdge | Qt.RightEdge)}
        }
    }
}
