import QtQuick
import QtQuick.Layouts
import QtQuick.Dialogs
import QtQuick.Controls.Material
import SERConv


ApplicationWindow {
    id: root
    visible: true
    title: "SERConv"
    minimumHeight: 350
    minimumWidth: 800
    width: 1100
    height: 500

    SerStyle {id: style}
    Material.background: style.backgroundColor

    property int bw: 2
    function toggleMaximized() {
        if (root.visibility === Window.Maximized) {
            root.showNormal();
        } else {
            root.showMaximized();
        }
    }

    // ------------------------ Dialogs --------------------------
    ConversionDialog {
        id: convertDialog

        Connections {
            target: Document

            function onConversionStarted() {
                convertDialog.visible = true
            }
        }
    }

    AboutDialog {
        id: aboutDialog
    }

    DropArea {
        anchors.fill: parent
        onDropped: (drop) => {
                       Document.addFromUrls(drop.urls)
                   }
    }


    // ----------------------- Content ------------------------------
    header: SerToolbar {
        id: tools
    }
    Item {
        id: content
        anchors.fill: parent
            SplitView {
                id: view
                anchors.fill: parent

                EntryContainer {
                    SplitView.preferredWidth: 750
                    SplitView.minimumWidth: 400
                    SplitView.fillWidth: true
                }

                OptionsPane {
                    SplitView.minimumWidth: 230
                }
            }
    }
}

