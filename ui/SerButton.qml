import QtQuick
import QtQuick.Controls.Material

Button {
    id: control

    property color backgroundColor: highlighted ? Material.accentColor : Material.theme === Material.Dark ? Qt.lighter(Material.backgroundColor, 1.3) : Qt.darker(Material.backgroundColor, 1.1)
    property color backgroundHoverColor: highlighted ? Qt.lighter(Material.accentColor, 1.1) : Material.theme === Material.Dark ? Qt.lighter(backgroundColor, 1.1) : Qt.darker(backgroundColor, 1.025)
    property color backgroundPressedColor: highlighted ? Qt.lighter(Material.accentColor, 1.2) :  Material.theme === Material.Dark ? Qt.lighter(backgroundColor, 1.2) : Qt.darker(backgroundColor, 1.05)

    signal clicked()
    property alias implicitWidth: rect.implicitWidth
    property alias radius: rect.radius

    onClicked: focus = true

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
//        propagateComposedEvents: true
//        preventStealing: true
        onClicked: parent.clicked()
        onEntered: {
            if (control.enabled)
                rect.color = pressed ? backgroundPressedColor : backgroundHoverColor
        }
        onExited: rect.color = pressed ? backgroundPressedColor : backgroundColor
        onPressed: rect.color = backgroundPressedColor
        onReleased: rect.color = backgroundColor
    }

    background: Rectangle {
        id: rect
        radius: 3
        color: highlighted ? Material.accentColor : backgroundColor
        implicitWidth: 75
    }
}

