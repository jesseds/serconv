import QtQuick
import QtQuick.Layouts
import QtQml.Models
import QtQuick.Controls.Material
import SERConv

Item {
    id: root

    SerStyle {id: style}

    Component {
        id: entryDelegate
        SerPane {
            id: delegatePane
            width: list.width - 6
            height: 60
            Material.elevation: 1
            border.width: 1
//            border.color: Material.theme === Material.light ? "lightgrey" : Qt.lighter(Material.backgroundColor, 1.3)
            border.color: style.backgroundDimColor2

            RowLayout {
                anchors.centerIn: parent
                width: parent.width
                CheckBox {
                    checked: active
                    onCheckedChanged: {
                        active = checked
                    }
                }
                ColumnLayout {
                    RowLayout {
                        spacing: 25
                        Label {text: name; font.pointSize: 13}
                        Label {text: descr; font.pointSize: 13; opacity: 0.3}
                    }
                    Label {text: path; opacity: 0.3; Layout.fillWidth: true}
                }

                Item {Layout.fillWidth: true}
                RoundButton {
                    flat: true
                    icon.source: "qrc:SERConv/icons/deleterows.png"
                    onClicked: {
                        Document.removeByAbsPath(path)
                    }
                }
            }
        }
    }

    ColumnLayout {
        id: layout
        spacing: 0
        anchors.fill: parent
        anchors.margins: 12

        Rectangle {
            id: bkg
            Layout.fillWidth: true
            Layout.fillHeight: true
            color: style.backgroundColor

            ListView {
                id: list
                clip: true
                model: Document.entryModel

                anchors.fill: parent
                leftMargin: 3
                rightMargin: 3
                bottomMargin: 3
                topMargin: 3
                spacing: 14
                remove: Transition {
                    NumberAnimation {property: "opacity"; to: 0; duration: 75}
                }

                displaced: Transition {
                    NumberAnimation {properties: "x,y"; duration: 150}
                }

                add: Transition {
                    NumberAnimation {property: "opacity"; from: 0;  to: 1; duration: 75}
                }

                ListView.onAdd: {
                    dragDropText.fade = false
                }
                onCountChanged: {
                    if (count === 0) {
                        dragDropText.fade = true
                    }
                    else {
                        dragDropText.fade = false
                    }
                }
                delegate: entryDelegate

            }
        }
    }

    Label {
        id: dragDropText
        text: "Drag & drop\nfiles or folders"
        anchors.centerIn: parent
        horizontalAlignment: Qt.AlignHCenter
        font.pointSize: 20
        property real baseOpacity: 0.13
        opacity: baseOpacity

        property bool fade: true
        states: [
            State {when: dragDropText.fade;
                PropertyChanges {target: dragDropText; opacity: baseOpacity}
            },
            State {when: !dragDropText.fade;
                PropertyChanges {target: dragDropText; opacity: 0.0}
            }
        ]

        transitions: Transition {
            NumberAnimation { property: "opacity"; duration: 100}
        }
    }
}
