import QtQuick
import QtQuick.Controls.Material

Item {
    property color backgroundColor: Material.theme === Material.Light ? "#f9f9f9" : "#38373a"
    property color backgroundDimColor1: Material.theme === Material.Light ? Qt.darker(Material.background, 1.03) : Qt.lighter(Material.background, 1.2)
    property color backgroundDimColor2: Material.theme === Material.Light ? Qt.darker(Material.background, 1.1) : Qt.lighter(Material.background, 1.4)
}
