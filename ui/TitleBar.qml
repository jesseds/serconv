import QtQuick
import QtQuick.Controls.Material
import QtQuick.Layouts
import SERConv

Item {
    id: root
    height: 40

    signal minimized()
    signal maximized()

    Rectangle {
        anchors.fill: parent
        color: Material.theme === Material.Light ? Qt.darker(Material.backgroundColor, 1.035) : Qt.lighter(Material.backgroundColor, 1.2)
//        radius: 8
    }

    RowLayout {
        anchors.fill: parent
        SerToolbar {Layout.fillHeight: true}

        Item {width: 10}
        Rectangle {height: parent.height*0.6; width: 1; color: "white"; opacity: 0.1}
        Item {width: 25}
        Label {text: "SERConv"; opacity: 0.5}
        Item {Layout.fillWidth: true}

        Rectangle {
            id: minimize
            color: "transparent"
            Layout.alignment: Qt.AlignTop
            height: 30
            width: 40
            Image {
                anchors.centerIn: parent
                source: "qrc:/SERConv/icons/minimize.svg"
            }
            MouseArea {
                propagateComposedEvents: true
                anchors.fill: minimize
                hoverEnabled: true
                pressAndHoldInterval: 0
                onEntered: {if (!pressed) minimize.color = Qt.darker("darkgrey", 1.8)}
                onPressAndHold: minimize.color = Qt.darker("darkgrey", 1.4)
                onExited: {if (!pressed) minimize.color = "transparent"}
                onReleased: (mouse) => {
                    if (containsMouse) {
                        minimized()
                    }
                    minimize.color = "transparent"
                }
            }
        }
        Rectangle {
            id: maximize
            color: "transparent"
            Layout.alignment: Qt.AlignTop
            height: 30
            width: 40
            Image {
                anchors.centerIn: parent
                source: "qrc:/SERConv/icons/maximize.svg"
            }
            MouseArea {
                propagateComposedEvents: true
                anchors.fill: maximize
                hoverEnabled: true
                pressAndHoldInterval: 0
                onEntered: {if (!pressed) maximize.color = Qt.darker("darkgrey", 1.8)}
                onPressAndHold: maximize.color = Qt.darker("darkgrey", 1.4)
                onExited: {if (!pressed) maximize.color = "transparent"}
                onReleased: (mouse) => {
                    if (containsMouse) {
                        maximized()
                    }
                    maximize.color = "transparent"
                }
            }
    }
    Rectangle {
            id: close
            color: "transparent"
//            Layout.fillHeight: true
            Layout.alignment: Qt.AlignTop
            height: 30
            width: 40
            Image {
                anchors.centerIn: parent
                source: "qrc:/SERConv/icons/close.svg"
            }
            MouseArea {
                propagateComposedEvents: true
                anchors.fill: close
                hoverEnabled: true
                pressAndHoldInterval: 0
                onEntered: {if (!pressed) close.color = Qt.lighter("red", 1.4)}
                onPressAndHold: close.color = Qt.lighter("red", 1.2)
                onExited: {if (!pressed) close.color = "transparent"}
                onReleased: {
                    if (containsMouse) {
                        Document.exitApp()
                    }
                    close.color = "transparent"
                }
            }
        }
    }
}
