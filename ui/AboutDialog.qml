import QtQuick
import QtQuick.Window
import QtQuick.Layouts
import QtQuick.Controls.Material
import QtQuick.Controls


Dialog {
    title: "About SERConv " + VERSION
    standardButtons: DialogButtonBox.Close
    parent: Overlay.overlay
    width: 400
    dim: false
    anchors.centerIn: parent
    modal: true

    TextArea {
        selectByMouse: true
        horizontalAlignment: Qt.AlignHCenter
        anchors.fill: parent
        text: "Copyright 2018-2022 Jesse Smith https://gitlab.com/jesseds/serconv\n\nSERConv is a program for batch conversion of TIA ser files to tiff.\n\nSERConv comes with ABSOLUTELY NO WARRANTY. NLM Denoiser is Free Software and you are entitled to distribute it under the terms of the GNU Public License (GPL). See the file LICENSE for details."
        readOnly: true
        background: null
        wrapMode: TextArea.WordWrap
    }
}
