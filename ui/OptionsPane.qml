import QtQuick
import QtQuick.Layouts
import QtQuick.Controls.Material
import SERConv

SerPane {
    Material.elevation: 12
    radius: 0
    ColumnLayout {
        anchors.fill: parent
        Text {
            text: "Options"
            font.pointSize: 16
            color: Material.accentColor
            Layout.alignment: Qt.AlignVCenter
        }
        Item {height: 0}
        Rectangle {Layout.fillWidth: true; color: Material.accentColor; height: 1; opacity: 0.2}
        TextField {
            id: suffix
            Layout.fillWidth: true
            placeholderText: "Suffix"
            onTextChanged: Document.suffix = text
        }

        Switch {
            id: subdirs
            hoverEnabled: true
            Layout.fillWidth: true
            text: "Search subfolders"
            checked: Document.subDirs
            onCheckedChanged: {
                Document.subDirs = checked
            }
        }
        Switch {
            id: to8bit
            Layout.fillWidth: true
            text: "Images to 8-bit"
            checked: Document.castTo8Bit
            onCheckedChanged: {
                Document.castTo8Bit = checked
            }
        }
        Switch {
            id: overwrite
            Layout.fillWidth: true
            text: "Overwrite existing"
            checked: Document.overwrite
            onCheckedChanged: {
                Document.overwrite = checked
            }
        }

        Item {Layout.fillHeight: true}
        SerButton {
            Layout.fillWidth: true
            text: "Convert \u27A4"
            highlighted: Material.theme === Material.Light ? true : false
//            backgroundColor: Material.theme === Material.Dark ? Material.color(Material.DeepPurple, Material.Shade300) : Material.accentColor
//            backgroundColor: Material.theme === Material.Dark ? Material.color(Material.DeepPurple, Material.Shade300) :
            Binding on backgroundColor {
                when: Material.theme === Material.Dark
                value: Material.color(Material.DeepPurple, Material.Shade300)
            }

//            backgroundColor: if (Material.theme === Material.Dark) Material.color(Material.DeepPurple, Material.Shade300)
//            backgroundColor: {
//                if (Material.theme === Material.Dark) {
//                    Material.color(Material.DeepPurple, Material.Shade300)
//                }
            implicitHeight: 50
            onClicked: {
                Document.convert()
            }
        }
    }
}
