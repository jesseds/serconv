import QtQuick
import QtQuick.Layouts
import QtQuick.Controls.Material
import Qt.labs.platform
import QtQuick.Dialogs
import SERConv

Rectangle {
    id: root
    SerStyle {id: style}
    height: 38
    color: style.backgroundDimColor1

    FolderDialog {
        id: folderDialog
        title: "Add folder containing ser files"
        property string lastdir: StandardPaths.writableLocation(StandardPaths.HomeLocation)
        currentFolder: lastdir
        onVisibleChanged: lastdir = folderDialog.currentFolder
        options: FolderDialog.ShowDirsOnly
        onAccepted: {
            if (selectedFolder) {
                Document.addFromUrls([selectedFolder])
            }
        }
    }

    FileDialog {
        id: fileDialog
        title: "Add ser file"
        property string lastdir: StandardPaths.writableLocation(StandardPaths.HomeLocation)
        currentFolder: lastdir
        onVisibleChanged: lastdir = folderDialog.currentFolder
        nameFilters: ["TIA ser files (*.ser)"]
        fileMode: FileDialog.OpenFiles
        onAccepted: {
            if (selectedFiles) {
                Document.addFromUrls(selectedFiles)
            }
        }
    }

    RowLayout {
        anchors.fill: parent

        Material.foreground: Material.accent
        SerToolButton {
            text: "Add folder"
            Layout.fillHeight: true
            icon.source: "qrc:SERConv/icons/folder.png"
            onClicked: folderDialog.visible = true
        }
        SerToolButton {
            text: "Add file(s)"
            Layout.fillHeight: true
            icon.source: "qrc:SERConv/icons/file.png"
            onClicked: fileDialog.visible = true
//            display: AbstractButton.TextUnderIcon
        }
        SerToolButton {
            text: "Clear"
            Layout.fillHeight: true
            icon.source: "qrc:SERConv/icons/clear_2.png"
            onClicked: Document.clearEntries()
        }
//        Item {Layout.fillWidth: true}
        SerToolButton {
            icon.source: "qrc:SERConv/icons/about.png"
            text: "About"
            Layout.fillHeight: true
            onClicked: {
                aboutDialog.visible = true
            }
        }
        Item {Layout.fillWidth: true}
//        Switch {text: "Dark";
//            Layout.fillHeight: true
//            onCheckedChanged: (checked) => {
//                                  if (Material.theme === Material.Dark) {Material.theme = Material.Light}
//                                  if (Material.theme === Material.Light) {Material.theme = Material.Dark}
//                              }
//        }
    }
    Rectangle {anchors.verticalCenter: root.bottom; width: root.width; anchors.left: root.left; height: 1; color: style.backgroundDimColor2}
}
